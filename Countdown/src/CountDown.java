import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CountDown implements Runnable{
	
	private static JFrame frame;
	private static JLabel output;
	
	private static JTextField yearsIn = new JTextField();
	private static JTextField daysIn = new JTextField();
	private static JTextField hoursIn = new JTextField();
	private static JTextField minIn = new JTextField();
	private static JTextField secIn = new JTextField();
	
	private static long years = 0;
	private static long days  = 0;
	private static long hours = 0;
	private static long min   = 0;
	private static long sec   = 0;
	
	private static Thread timer = null;
	
	public static void main(String args[]){
		Dimension scr = Toolkit.getDefaultToolkit().getScreenSize();
		final JFrame settings = new JFrame();
		GridLayout layout = new GridLayout(6,2);
		settings.setLayout(layout);
		settings.add(new JLabel("Years:   "));
		settings.add(yearsIn);
		settings.add(new JLabel("Days:    "));
		settings.add(daysIn);
		settings.add(new JLabel("Hours:   "));
		settings.add(hoursIn);
		settings.add(new JLabel("Minutes: "));
		settings.add(minIn);
		settings.add(new JLabel("Seconds: "));
		settings.add(secIn);
		JButton credit = new JButton("Info");
		credit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(settings, "Count Down Timer\n\nDeveloper: Chris Colahan (christopher.colahan@gmail.com)\n\nFree to use and reditribute as long as unchanged.\nContact developer for questions.", "Credits", JOptionPane.PLAIN_MESSAGE);
			}
		});
		settings.add(credit);
		JButton start = new JButton("Start");
		start.addActionListener(
			new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(timer != null){
						timer.interrupt();
						timer = null;
					}
					if(!yearsIn.getText().equals("")){
						years = Integer.parseInt(yearsIn.getText());
					}
					else{
						years = 0;
					}
					if(!daysIn.getText().equals("")){
						days = Integer.parseInt(daysIn.getText());
					}
					else{
						days = 0;
					}
					if(!hoursIn.getText().equals("")){
						hours = Integer.parseInt(hoursIn.getText());
					}
					else{
						hours = 0;
					}
					if(!minIn.getText().equals("")){
						min = Integer.parseInt(minIn.getText());
					}
					else{
						min = 0;
					}
					if(!secIn.getText().equals("")){
						sec = Integer.parseInt(secIn.getText());
					}
					else{
						sec = 0;
					}
					
					timer = new Thread(new CountDown(),"COUNTER");
					timer.start();
				}
			});
		settings.add(start);
		settings.setTitle("Settings");
		settings.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		settings.setBounds(0, 0, 200, 200);
		settings.setResizable(false);
		settings.setVisible(true);
		
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension window = new Dimension(550, 125);
		frame.setBounds(scr.width/2-window.width/2,
				scr.height/2-window.height/2, window.width, window.height);
		output = new JLabel();
		output.setVisible(true);
		output.setFont(new Font(output.getFont().getName(),
				output.getFont().getStyle(),72));
		output.setText("00:000:00:00:00");
		frame.setLayout(new BorderLayout());
		frame.getContentPane().add(BorderLayout.CENTER,output);
		frame.setTitle("Count Down");
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	@Override
	public void run() {
		
		final long miliPerSec = 1000;
		final long miliPerMin = 60*miliPerSec;
		final long miliPerHour = 60*miliPerMin;
		final long miliPerDay = 24*miliPerHour;
		final long miliPerYear = (long)(365.25*((double)miliPerDay));
		
		long end = System.currentTimeMillis() + 
				(years*miliPerYear) + (days*miliPerDay) 
				+ (hours*miliPerHour) + (min*miliPerMin)
				+ (sec*miliPerSec);
		
		DecimalFormat time = new DecimalFormat("00");
		DecimalFormat daysFormat = new DecimalFormat("000");
		
		while(true){
			try {
				long timeLeft = end - System.currentTimeMillis();
				long yearsLeft = timeLeft/miliPerYear;
				timeLeft %= miliPerYear;
				long daysLeft = timeLeft/miliPerDay;
				timeLeft %= miliPerDay;
				long hoursLeft = timeLeft/miliPerHour;
				timeLeft %= miliPerHour;
				long minLeft = timeLeft/miliPerMin;
				timeLeft %= miliPerMin;
				long secLeft = timeLeft/miliPerSec;
				timeLeft %= miliPerSec;
				String out = time.format(yearsLeft)
						+":"+daysFormat.format(daysLeft)+":"+
						time.format(hoursLeft)+":"+time.format(minLeft)
						+":"+time.format(secLeft);
				if(secLeft == 0 && minLeft == 0 && hoursLeft == 0
						&& daysLeft == 0 && yearsLeft == 0){
					output.setText("00:000:00:00:00");
					JOptionPane.showMessageDialog(frame, "TIME UP", "Time Up", JOptionPane.PLAIN_MESSAGE);
					break;
				}
				output.setText(out);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				break;
			}
			
		}
	}
}
